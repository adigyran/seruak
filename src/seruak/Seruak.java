package seruak;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JSlider;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import static java.lang.Math.toIntExact;
import javax.swing.JCheckBox;
import jssc.SerialPortList;
import static seruak.Seruak.cal;

public class Seruak {
    //static serialframe flo;
    private static SerialPort serialPort;
   static serialframe cal;
  static Integer stepsper;
  Logger logdd_main;
    public Seruak()
    {
       //flo = new serialframe();
        //flo.setVisible(true);
        
    }
    
    public static void main(String[] args) {
        //Передаём в конструктор имя порта
       cal=new serialframe();
       stepsper=100;
       System.out.println("Available COM ports: ");
      String[] portNames = SerialPortList.getPortNames();
      if(portNames.length==0)
      {
          System.out.println("No avaliable ports. Connect device and restart the app");
          System.exit(0);
      }
        for(int i = 0; i < portNames.length; i++){
            System.out.println(i+". "+portNames[i]);
        }
        System.out.println("Enter comport number  or (\"exit\" or \"quit\" for quit) ");
        Scanner s = new Scanner(System.in);
        String comportnm = s.next();
        boolean notan = false;
        //String comport = portNames[Integer.parseInt(comportnm)];
        int comportnmi=999999999;
        try{
         comportnmi = Integer.parseInt(comportnm);
         
     }catch(NumberFormatException the_input_string_isnt_an_integer){
         notan = true;
  // ask the user to try again
     }
         if (comportnm.equals("quit")||comportnm.equals("exit"))
        {
        
            System.exit(0);
        }
         else if(notan==true)
         {
             System.out.println("You entered wrong command");
             System.exit(0);
         }
    
         if(comportnmi>portNames.length || notan==true)
       {
           System.out.println("Wrong port");
           System.exit(0);
       }else
         {
        String comport = portNames[comportnmi];
       
     
        //</editor-fold>
       // Calculas.setNimbusFeel();
       /* Create and display the form */
       java.awt.EventQueue.invokeLater(() -> {
           cal.jButton1.addActionListener((java.awt.event.ActionEvent evt) -> {
               try {
                   serialwritse(serialPort,1000,"B1");
               } catch (SerialPortException ex) {
                   Logger.getLogger(Seruak.class.getName()).log(Level.SEVERE, null, ex);
               }
           });
           cal.jButton3.addActionListener((java.awt.event.ActionEvent evt) -> {
               try {
                   int servorpm = Integer.parseInt(cal.servo_rpmnonformat.getText().replaceAll(",", ""));
                   serialwritse(serialPort,(int)servorpm,"S2");
               } catch (SerialPortException ex) {
                   Logger.getLogger(Seruak.class.getName()).log(Level.SEVERE, null, ex);
               }
           });
           cal.jSlider_pump.addChangeListener((javax.swing.event.ChangeEvent evt) -> {
               try {
                   JSlider source = (JSlider)evt.getSource();
                   cal.changelavel2(String.valueOf((int)source.getValue()));
                   serialwritse(serialPort,(int)source.getValue(),"S1");
               } catch (SerialPortException ex) {
                   Logger.getLogger(Seruak.class.getName()).log(Level.SEVERE, null, ex);
               }
           });
           cal.jSlider_servo.addChangeListener((javax.swing.event.ChangeEvent evt) -> {
               try {
                   JSlider source = (JSlider)evt.getSource();
                    cal.changelavel3(String.valueOf((int)source.getValue()));
                   serialwritse(serialPort,(int)source.getValue(),"S2");
               } catch (SerialPortException ex) {
                   Logger.getLogger(Seruak.class.getName()).log(Level.SEVERE, null, ex);
               }
           });
           cal.jCheckBox_servoenable.addItemListener((java.awt.event.ItemEvent  evt) -> {    
               try{
     JCheckBox soruce = (JCheckBox)evt.getSource(); 
     cal.logdd.info(Boolean.toString(soruce.isSelected()));
     serialwritsebool(serialPort,soruce.isSelected(),"E1");
   //   serialwritse(serialPort,soruce.isSelected(),"S2");
             } catch (SerialPortException ex) {
              //  Logger.getLogger(Seruak.class.getName()).log(Level.SEVERE, null, ex);
                cal.logdd.log(Level.SEVERE, null, ex);
            }
               
               
    //logdd.info(Boolean.toString(soruce.isSelected()));
     //jCheckBox1.setSelected(soruce.isSelected());
    });
               cal.jCheckBox2.addItemListener((java.awt.event.ItemEvent  evt) -> {    
               try{
     JCheckBox soruce = (JCheckBox)evt.getSource(); 
     cal.logdd.info(Boolean.toString(soruce.isSelected()));
     serialwritsebool(serialPort,soruce.isSelected(),"D1");
   //   serialwritse(serialPort,soruce.isSelected(),"S2");
             } catch (SerialPortException ex) {
              //  Logger.getLogger(Seruak.class.getName()).log(Level.SEVERE, null, ex);
                cal.logdd.log(Level.SEVERE, null, ex);
            }
               
               
    //logdd.info(Boolean.toString(soruce.isSelected()));
     //jCheckBox1.setSelected(soruce.isSelected());
    });
           cal.setVisible(true);
        });
      
        //serialPort = new SerialPort("COM15");
         serialPort = new SerialPort(comport);
        try {
            //Открываем порт
            serialPort.openPort();
            //Выставляем параметры
            serialPort.setParams(SerialPort.BAUDRATE_9600,
                                 SerialPort.DATABITS_8,
                                 SerialPort.STOPBITS_1,
                                 SerialPort.PARITY_NONE);
            //Включаем аппаратное управление потоком
            serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN | 
                                          SerialPort.FLOWCONTROL_RTSCTS_OUT);
            //Устанавливаем ивент лисенер и маску
            serialPort.addEventListener(new PortReader(), SerialPort.MASK_RXCHAR);
            //Отправляем запрос устройству
         //   serialPort.writeString("Get data");
        }
        catch (SerialPortException ex) {
            System.out.println(ex);
        }
         }
    }
private void sirot()
{
  
}
 private static void serialwritsebool(SerialPort serialport, boolean propert, String numberr ) throws SerialPortException{
           if(serialPort != null && serialPort.isOpened()){
                    try {
                       // String stringOut = textFieldOut.getText();
                        serialport.writeBytes(">".getBytes());
                      // String dlr = propert
                        
                        serialport.writeBytes((numberr+Boolean.toString(propert)+"\n").getBytes());
                      //  System.out.println("Something wrongs!");
                    } catch (SerialPortException ex) {
                        
                    }
                }else{
                    System.out.println("Something wrong with serial write!");
                }
            
        }
 private static void serialwritse(SerialPort serialport, int propert, String numberr ) throws SerialPortException{
           if(serialPort != null && serialPort.isOpened()){
                    try {
                       // String stringOut = textFieldOut.getText();
                        serialport.writeBytes(">".getBytes());
                      // String dlr = propert
                        
                        serialport.writeBytes((numberr+String.valueOf(propert)+"\n").getBytes());
                      //  System.out.println("Something wrongs!");
                    } catch (SerialPortException ex) {
                        
                    }
                }else{
                    System.out.println("Something wrong with serial write!");
                }
            
        }

 
    private static class PortReader implements SerialPortEventListener {
  StringBuilder message = new StringBuilder();
  
        @Override
        public void serialEvent(SerialPortEvent event) {
            if(event.isRXCHAR() && event.getEventValue() > 0){
                try {
                       byte buffer[] = serialPort.readBytes();
            for (byte b: buffer) {
                    if ( (b == '\r' || b == '\n') && message.length() > 0) {
                       //   serialPort.writeBytes(">".getBytes());
                        String toProcess = message.toString();
                       //Получаем ответ от устройства, обрабатываем данные и т.д.
                   // String data = serialPort.readString(event.getEventValue());
                    System.out.println(toProcess);
                     cal.changelavel(toProcess);
                    //И снова отправляем запрос
                   //  byte lof = '>';
                  
                        message.setLength(0);
                    }
                    else {
                        message.append((char)b);
                    }
            }    
                    
                   
                }
                catch (SerialPortException ex) {
                    System.out.println(ex);
                }
            }
        }
       
    }
    
}